DROP DATABASE IF EXISTS SQLAdvanced201;
GO

CREATE DATABASE SQLAdvanced201;
GO

USE SQLAdvanced201;
GO

/* 2a. Create the tables (with the most appropriate field/column constraints & types) and add at least 3 records into each created table. */
CREATE TABLE Employees (
	EmployeeID int PRIMARY KEY,
	EmployeeFirstName nvarchar(50) NOT NULL,
	EmployeeLastName nvarchar(100) NOT NULL,
	EmployeeHireDate date NOT NULL,
	EmployeeStatus int NOT NULL Default(0),
	SupervisorID int,
	SocialSecurityNumber nvarchar(20) NOT NULL
);
GO

ALTER TABLE Employees
ADD CONSTRAINT FK_Employees_Employees_SupervisorID FOREIGN KEY (SupervisorID)
REFERENCES Employees(EmployeeID)
GO

CREATE TABLE Projects (
	ProjectID int PRIMARY KEY,
	ProjectName nvarchar(100) NOT NULL,
	ProjectStartDate date NOT NULL,
	ProjectDescription text,
	ProjectDetail text,
	ProjectCompletedOn date NULL,
	ProjectManagerID int UNIQUE FOREIGN KEY REFERENCES Employees(EmployeeID) NOT NULL
);
GO

CREATE TABLE ProjectsModules (
	ModuleID int PRIMARY KEY,
	ProjectID int FOREIGN KEY REFERENCES Projects(ProjectID) NOT NULL,
	EmployeeID int FOREIGN KEY REFERENCES Employees(EmployeeID) NOT NULL,
	ProjectModulesDate date NOT NULL,
	ProjectModulesCompletedOn date,
	ProjectModulesDescription text
);
GO

CREATE TABLE WorkDone (
	WorkDoneID int PRIMARY KEY,
	EmployeeID int NOT NULL FOREIGN KEY REFERENCES Employees(EmployeeID),
	ModuleID int NOT NULL FOREIGN KEY REFERENCES ProjectsModules(ModuleID),
	WorkDoneDate date,
	WorkDoneDescription text,
	WorkDoneStatus int NOT NULL
);
GO

INSERT INTO Employees (EmployeeID, EmployeeFirstName, EmployeeLastName, EmployeeHireDate, SupervisorID, SocialSecurityNumber)
VALUES (1, 'Nguyen', 'An', '10-21-2019', NULL, 'SSN1'),
	(2, 'Tran', 'Binh', '09-25-2021', 1, 'SSN2'),
	(3, 'Pham', 'Chau', '05-27-2019', 1, 'SSN3'),
	(4, 'Do', 'Dat', '01-15-2019', 2, 'SSN4'),
	(5, 'To', 'Giang', '11-21-2019', 2, 'SSN5')
GO

INSERT INTO Projects (ProjectID, ProjectName, ProjectStartDate, ProjectDescription, ProjectDetail, ProjectManagerID)
VALUES (1, 'Project 1', '06-20-2020', 'Project description 1', 'Project detail 1', 1),
	(2, 'Project 2', '07-26-2021', 'Project description 2', 'Project detail 2', 2),
	(3, 'Project 3', '12-11-2020', 'Project description 3', 'Project detail 3', 3),
	(4, 'Project 4', '05-12-2019', 'Project description 4', 'Project detail 4', 4),
	(5, 'Project 5', '06-30-2022', 'Project description 5', 'Project detail 5', 5)
GO


INSERT INTO ProjectsModules (ModuleID, ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesDescription)
VALUES (1, 1, 1, '06-20-2020', 'Some description'),
	(2, 1, 2, '06-20-2020', 'Some description'),
	(3, 2, 3, '07-26-2021', 'Some description'),
	(4, 2, 4, '07-26-2021', 'Some description'),
	(5, 3, 5, '06-20-2020', 'Some description'),
	(6, 4, 5, '06-20-2020', 'Some description')
GO

INSERT INTO WorkDone(WorkDoneID, ModuleID, EmployeeID, WorkDoneStatus)
VALUES (1, 1, 1, 0),
	(2, 1, 2, 0),
	(3, 2, 1, 0),
	(4, 2, 3, 0),
	(5, 3, 4, 0),
	(6, 4, 5, 0)
GO

/* 2b. Write a stored procedure (with parameter) to print out the modules that a specific employee has been working on. */
CREATE PROC WorkingModules
	@EmployeeName NVARCHAR(151)
AS
BEGIN
	SELECT *
	FROM ProjectsModules PM
	INNER JOIN Employees E
	ON PM.EmployeeID = E.EmployeeID
	WHERE E.EmployeeFirstName + ' ' + E.EmployeeLastName = @EmployeeName
END;
GO

EXECUTE WorkingModules 'Nguyen An'
GO

/* 2c. Write a user function (with parameter) that return the Works information that a specific employee has been involving though those were not assigned to him/her. */
CREATE FUNCTION WorkingModulesByEmployee(@EmployeeName nvarchar(151))
RETURNS TABLE
AS
RETURN (
	SELECT *
	FROM WorkDone WD
	WHERE WD.EmployeeID = (
		SELECT E.EmployeeID
		FROM Employees E
		WHERE E.EmployeeFirstName + ' ' + E.EmployeeLastName = @EmployeeName)
)
GO

SELECT * FROM WorkingModulesByEmployee('Nguyen An')
GO

SELECT * FROM Projects
GO
/* 2d. Write the trigger(s) to prevent the case that the end user to input invalid Projects and Project Modules information */

CREATE TRIGGER ValidateProjectModulesBeforeInsert
ON ProjectsModules
INSTEAD OF INSERT
AS
BEGIN
	IF EXISTS (
		SELECT 1
		FROM inserted I
		INNER JOIN Projects P
		ON I.ProjectID = P.ProjectID
		WHERE I.ProjectModulesDate < P.ProjectStartDate OR
			I.ProjectModulesCompletedOn > P.ProjectCompletedOn
		)
	BEGIN
		RAISERROR ('Invalid ProjectModulesDate or invalid ProjectModulesCompletedOn', 16, 1);
		ROLLBACK TRANSACTION;
	END
	ELSE
	BEGIN
		INSERT INTO ProjectsModules
		SELECT *
		FROM inserted
	END
END
GO


CREATE TRIGGER ValidateProjectModuleBeforeUpdate
ON ProjectsModules
INSTEAD OF UPDATE
AS
BEGIN
	IF EXISTS (
		SELECT 1
		FROM inserted I
		INNER JOIN Projects P
		ON I.ProjectID = P.ProjectID
		WHERE  I.ProjectModulesDate < P.ProjectStartDate OR
			I.ProjectModulesCompletedOn > P.ProjectCompletedOn
		)
	BEGIN
		RAISERROR ('Invalid ProjectModulesDate or invalid ProjectModulesCompletedOn', 16, 1);
		ROLLBACK TRANSACTION;
	END
	ELSE
	BEGIN
		UPDATE ProjectsModules
		SET
			ProjectModulesDate = I.ProjectModulesDate,
			ProjectModulesCompletedOn = I.ProjectModulesCompletedOn,
			ProjectModulesDescription = I.ProjectModulesDescription
		FROM ProjectsModules PM
		INNER JOIN inserted I
		ON PM.ModuleID = I.ModuleID
	END
END
GO

INSERT INTO ProjectsModules (ModuleID, ProjectID, EmployeeID, ProjectModulesDate, ProjectModulesDescription)
VALUES (7, 1, 2, '06-15-2020', 'Some description')
